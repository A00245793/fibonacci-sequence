import junit.framework.TestCase;


public class FibonacciGeneratorTest extends TestCase {

		/*
		 * Test: 	1
		 * Obj:		Valid input - positive number
		 * Input:	1
		 * Exp o/p:	True
		 */
	
	public void validateNTest001(){
		
		FibonacciGenerator testObj = new FibonacciGenerator();
		
		assertEquals(true, testObj.validateN(1));
	}
	
		/*
		 * Test: 	2
		 * Obj:		Invalid input - negative number
		 * Input:	-2
		 * Exp o/p:	False
		 */
	
	public void validateNTest002(){
		
		FibonacciGenerator testObj = new FibonacciGenerator();
		
		assertEquals(false, testObj.validateN(-2));
	}
	
	
		/*
		 * Test: 	3
		 * Obj:		f(1) = 1
		 * Input:	1
		 * Exp o/p:	1
		 */
	
	public void calculateNthValueTest003(){
		
		FibonacciGenerator testObj = new FibonacciGenerator();
		
		assertEquals(1, testObj.calculateNthValue(1));
	}
	
		/*
		 * Test: 	4
		 * Obj:		f(2) = 1
		 * Input:	2
		 * Exp o/p:	1
		 */
		
	public void calculateNthValueTest004(){
		
		FibonacciGenerator testObj = new FibonacciGenerator();
		
		assertEquals(1, testObj.calculateNthValue(2));
	}
	
		/*
		 * Test: 	5
		 * Obj:		f(n) = f(n-1) + f(n-2)
		 * Input:	6
		 * Exp o/p:	8
		 */
	
	public void calculateNthValueTest005(){
		
		FibonacciGenerator testObj = new FibonacciGenerator();
		
		assertEquals(8, testObj.calculateNthValue(5));
	}
	
	
	
}
